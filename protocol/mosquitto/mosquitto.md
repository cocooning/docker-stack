---
title: mosquitto
description: 
published: true
date: 2022-09-16T06:03:41.796Z
tags: 
editor: markdown
dateCreated: 2022-09-07T05:05:15.348Z
---
# mosquitto
- Version : 2.0.18

## Pré-requis
### Création des repertoires
```bash
/.cocooning/data/mosquitto/data
/.cocooning/data/mosquitto/log
```
### Le partage NFS 
```bash
sudo nano /etc/exports
```

```bash
/.cocooning/data/mosquitto/data *(rw,no_root_squash,sync,no_subtree_check,insecure)
/.cocooning/data/mosquitto/log *(rw,no_root_squash,sync,no_subtree_check,insecure)
```
```bash
sudo exportfs -ra
sudo service nfs-kernel-server reload
```
### Copy du mot de passe
Utiliser __WinSCP__ et recopier le password d'un autre utilisateur
> Changer cette manière de faire
### Les variables d'environnement
```bash
${ENV_SYSLOG_IP}
${ENV_MOSQUITTO_NODE_DEPLOY}
```

## Configuration

```bash
docker config create --template-driver golang mosquitto /.cocooning/docker-stack/protocol/mosquitto/mosquitto.conf
```
## Installation

```bash
docker stack deploy --compose-file /.cocooning/docker-stack/protocol/mosquitto/mosquitto-docker-compose.yml mosquitto
```
## Post-installation

### Création d'un nouveau mot de passe
Dans le container, au premier lancement créer le mot de passe hashé.

```bash
mosquitto_passwd -c /mosquitto/data/mosquitto.passwd <username>
```
