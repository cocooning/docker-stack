---
title: zigbee
description: 
published: true
date: 2022-09-16T06:03:41.796Z
tags: 
editor: markdown
dateCreated: 2022-09-07T05:05:15.348Z
---
# zigbee
> Sécuriser (pan_id, channel) avec des variables d'environnement

> Flash sur le worker1 / se mettre en sudo / parfois redemarrer le worker1 / Parfois changer de canal 11 à xx pour redemarrer proprement de controleur

## Pré-requis
- NFS server 
```bash
/.cocooning/data/zigbee
```

Ou Bind ou external volume (décrire et tester)

- Contôleur zigbee installé (voir plus bas)

## Les variables d'environnement
```bash
${ENV_TZ}
${ENV_SYSLOG_IP}
${ENV_MOSQUITTO_NODE_DEPLOY}
${ENV_ZIGBEE_ADAPTER}
```
## Configuration
```bash
Directement dans le data : copier/coller docker-stack/configuration.yaml dans data/zigbee
```
## Installation

> penser à supprimer le volume NFS Docker avant de redeployer la stack

```bash
docker stack deploy --compose-file /.cocooning/docker-stack/protocol/zigbee/zigbee-docker-compose.yml zigbee
```

## Web-UI

Activer la disponibilité après le premier lancement

Copier

```bash
availability:
  active:
    timeout: 20
  passive:
    timeout: 240
```
Dans le fichier __/.cocooning/data/zigbee/configuration.yaml__  Ce fichier est sous __data__ pour des raisons de sécurité.

## Command line
### Ajouter un matériel inconnu

# Installation d'un contrôleur zigbee
## C2531 USB sniffer

<pre><code>sudo lsusb -v
</code></pre>

Create a new udev rule for cc2531, idVendor and idProduct must be equal to values from lsusb command. The rule below creates device /dev/cc2531:

<pre><code>echo "SUBSYSTEM==\"tty\", ATTRS{idVendor}==\"0451\", ATTRS{idProduct}==\"16a8\", SYMLINK+=\"cc2531\",  RUN+=\"/usr/local/bin/docker-setup-cc2531.sh\"" | sudo tee /etc/udev/rules.d/99-cc2531.rules
</code></pre>
Reload newly created rule using the following command:
<pre><code>sudo udevadm control --reload-rules
</code></pre>
Create docker-setup-cc2531.sh
<pre><code>sudo nano /usr/local/bin/docker-setup-cc2531.sh
</code></pre>
Copy the following content:
<pre><code>#!/bin/bash
USBDEV=`readlink -f /dev/cc2531`
read minor major < <(stat -c '%T %t' $USBDEV)
if [[ -z $minor || -z $major ]]; then
    echo 'Device not found'
    exit
fi
dminor=$((0x${minor}))
dmajor=$((0x${major}))
CID=`docker ps -a --no-trunc | grep koenkk/zigbee2mqtt | head -1 |  awk '{print $1}'`
if [[ -z $CID ]]; then
    echo 'CID not found'
    exit
fi
echo 'Setting permissions'
echo "c $dmajor:$dminor rwm" > /sys/fs/cgroup/devices/docker/$CID/devices.allow
</code></pre>
Set permissions:
<pre><code>sudo chmod 744 /usr/local/bin/docker-setup-cc2531.sh
</code></pre>
Create docker-event-listener.sh
<pre><code>sudo nano /usr/local/bin/docker-event-listener.sh
</code></pre>
Copy the following content:
<pre><code>#!/bin/bash
docker events --filter 'event=start'| \
while read line; do
    /usr/local/bin/docker-setup-cc2531.sh
done
</code></pre>
Set permissions:
<pre><code>sudo chmod 744 /usr/local/bin/docker-event-listener.sh
</code></pre>
Create docker-event-listener.service
<pre><code>sudo nano /etc/systemd/system/docker-event-listener.service
</code></pre>
Copy the following content:
<pre><code>[Unit]
Description=Docker Event Listener for TI CC2531 device
After=network.target
StartLimitIntervalSec=0
[Service]
Type=simple
Restart=always
RestartSec=1
User=root
ExecStart=/bin/bash /usr/local/bin/docker-event-listener.sh

[Install]
WantedBy=multi-user.target
</code></pre>
Set permissions:
<pre><code>sudo chmod 744 /etc/systemd/system/docker-event-listener.service
</code></pre>
Reload daemon
<pre><code>sudo systemctl daemon-reload
</code></pre>
Start Docker event listener
<pre><code>sudo systemctl start docker-event-listener.service
</code></pre>
Status Docker event listener
<pre><code>sudo systemctl status docker-event-listener.service
</code></pre>
>active (running)

Enable Docker event listener
<pre><code>sudo systemctl enable docker-event-listener.service
</code></pre>
Verify Zigbee2MQTT
Reconect USB sniffer
<pre><code>ls -al /dev/cc2531
</code></pre>
>lrwxrwxrwx 1 root root 7 Sep 28 21:14 /dev/cc2531 -> ttyACM0

## Zigbee C2652 USB sniffer

<h2 style="color:#5434D7">Flash sur un raspberry dédié</h2>

<h3 style="color:#5434D7">Installer les outils</h3>

<h4 style="color:#5434D7">Sous windows</h4>

Utiliser __Flash programmer 2__ de texas instrument
Ne pas oublier de se connecter à la clef après l'avoir recherchée

<h4 style="color:#5434D7">Sous linux</h4>

```bash
sudo apt update 
sudo apt-get install python3-pip

# Télécharger le firmware sur le site zigbee2mqtt
wget https://raw.githubusercontent.com/Koenkk/Z-Stack-firmware/master/coordinator/Z-Stack_3.x.0/bin/CC2652R_coordinator_20210120.zip
unzip CC2652R_coordinator_20210120.zip

# Télécharger l'outil pour flasher
wget -O cc2538-bsl.zip https://codeload.github.com/JelmerT/cc2538-bsl/zip/master
unzip cc2538-bsl.zip

sudo pip3 install pyserial intelhex
```

<h3 style="color:#5434D7">Flasher le firmware</h3>

* Identifier le port  
* Verifier la liste la clef branchée

```bash
ls /dev/tty*
```

* Retirer la clef et relancer la commande pour identifier le "tty" qui disparait (/dev/ttyUSB0)
* Débranchez votre clé de l'hôte  
* Appuyez sur le BSLbouton poussoir et maintenez-le enfoncé tout en rebranchant l'appareil sur l'hôte  
* Donnez-lui quelques secondes pour que l'appareil se stabilise et configure et relâchez le bouton BSL

```bash
# Commande à lancer
# python3 cc2538-bsl.py -p PORT -evw FIRMWARE

cp CC2652R_coordinator_20210120.hex /home/tchube/cc2538-bsl-master
cd /home/tchube/cc2538-bsl-master
sudo python3 cc2538-bsl.py -p /dev/ttyUSB0 -evw CC2652R_coordinator_20210120.hex
```

<h2 style="color:#5434D7">Zigbee2mqtt</h2>

```bash
sudo lsusb -v
```

* Create a new udev rule for cc2652, __idVendor__ and __idProduct__ must be equal to values from lsusb command. The rule below creates device /dev/cc2652:

```bash
echo "SUBSYSTEM==\"tty\", ATTRS{idVendor}==\"1a86\", ATTRS{idProduct}==\"7523\", SYMLINK+=\"cc2652\",  RUN+=\"/usr/local/bin/docker-setup-cc2652.sh\"" | sudo tee /etc/udev/rules.d/98-cc2652.rules
```

* Reload newly created rule using the following command:

```bash
sudo udevadm control --reload-rules
```

* Create docker-setup-cc2652.sh

```bash
sudo nano /usr/local/bin/docker-setup-cc2652.sh
```

* Copy the following content:

```bash
#!/bin/bash
USBDEV=`readlink -f /dev/cc2652`
read minor major < <(stat -c '%T %t' $USBDEV)
if [[ -z $minor || -z $major ]]; then
    echo 'Device not found'
    exit
fi
dminor=$((0x${minor}))
dmajor=$((0x${major}))
CID=`docker ps -a --no-trunc | grep koenkk/zigbee2mqtt | head -1 |  awk '{print $1}'`
if [[ -z $CID ]]; then
    echo 'CID not found'
    exit
fi
echo 'Setting permissions'
echo "c $dmajor:$dminor rwm" > /sys/fs/cgroup/devices/docker/$CID/devices.allow
```

* Set permissions:

```bash
sudo chmod 744 /usr/local/bin/docker-setup-cc2652.sh
```

* Create docker-event-listener-cc2652.sh

```bash
sudo nano /usr/local/bin/docker-event-listener-cc2652.sh
```

* Copy the following content:

```bash
#!/bin/bash
docker events --filter 'event=start'| \
while read line; do
    /usr/local/bin/docker-setup-cc2652.sh
done
```

* Set permissions:

```bash
sudo chmod 744 /usr/local/bin/docker-event-listener-cc2652.sh
```

* Create docker-event-listener.service

```bash
sudo nano /etc/systemd/system/docker-event-listener-cc2652.service
```

* Copy the following content:

```bash
[Unit]
Description=Docker Event Listener for TI cc2652 device
After=network.target
StartLimitIntervalSec=0
[Service]
Type=simple
Restart=always
RestartSec=1
User=root
ExecStart=/bin/bash /usr/local/bin/docker-event-listener-cc2652.sh

[Install]
WantedBy=multi-user.target
```

* Set permissions:

```bash
sudo chmod 744 /etc/systemd/system/docker-event-listener-cc2652.service
```

* Reload daemon

```bash
sudo systemctl daemon-reload
```

* Start Docker event listener

```bash
sudo systemctl start docker-event-listener-cc2652.service
```

* Status Docker event listener

```bash
sudo systemctl status docker-event-listener-cc2652.service
```

>active (running)

* Enable Docker event listener

```bash
sudo systemctl enable docker-event-listener-cc2652.service
```

* Débrancher et reconnecter USB sniffer
* Verify Zigbee2MQTT

```bash
ls -al /dev/cc2652
```

>lrwxrwxrwx 1 root root 7 déc.   9 18:05 /dev/cc2652 -> ttyUSB0

## Installation d'un contrôleur Z-wave Aeotec Gen5 USB sniffer

<pre><code>sudo lsusb -v
</code></pre>

Create a new udev rule for zsgen5, idVendor and idProduct must be equal to values from lsusb command. The rule below creates device /dev/zsgen5:

<pre><code>echo "SUBSYSTEM==\"tty\", ATTRS{idVendor}==\"0658\", ATTRS{idProduct}==\"0200\", SYMLINK+=\"zsgen5\",  RUN+=\"/usr/local/bin/docker-setup-zsgen5.sh\"" | sudo tee /etc/udev/rules.d/97-zsgen5.rules
</code></pre>
Reload newly created rule using the following command:
<pre><code>sudo udevadm control --reload-rules
</code></pre>
Create docker-setup-zsgen5.sh
<pre><code>sudo nano /usr/local/bin/docker-setup-zsgen5.sh
</code></pre>
Copy the following content:
<pre><code>#!/bin/bash
USBDEV=`readlink -f /dev/zsgen5`
read minor major < <(stat -c '%T %t' $USBDEV)
if [[ -z $minor || -z $major ]]; then
    echo 'Device not found'
    exit
fi
dminor=$((0x${minor}))
dmajor=$((0x${major}))
CID=`docker ps -a --no-trunc | grep robertslando/zwave2mqtt | head -1 |  awk '{print $1}'`
if [[ -z $CID ]]; then
    echo 'CID not found'
    exit
fi
echo 'Setting permissions'
echo "c $dmajor:$dminor rwm" > /sys/fs/cgroup/devices/docker/$CID/devices.allow
</code></pre>
Set permissions:
<pre><code>sudo chmod 744 /usr/local/bin/docker-setup-zsgen5.sh
</code></pre>
Create docker-event-listener-zsgen5.sh
<pre><code>sudo nano /usr/local/bin/docker-event-listener-zsgen5.sh
</code></pre>
Copy the following content:
<pre><code>#!/bin/bash
docker events --filter 'event=start'| \
while read line; do
    /usr/local/bin/docker-setup-zsgen5.sh
done
</code></pre>
Set permissions:
<pre><code>sudo chmod 744 /usr/local/bin/docker-event-listener-zsgen5.sh
</code></pre>
Create docker-event-listener.service
<pre><code>sudo nano /etc/systemd/system/docker-event-listener-zsgen5.service
</code></pre>
Copy the following content:
<pre><code>[Unit]
Description=Docker Event Listener for TI zsgen5 device
After=network.target
StartLimitIntervalSec=0
[Service]
Type=simple
Restart=always
RestartSec=1
User=root
ExecStart=/bin/bash /usr/local/bin/docker-event-listener-zsgen5.sh

[Install]
WantedBy=multi-user.target
</code></pre>
Set permissions:
<pre><code>sudo chmod 744 /etc/systemd/system/docker-event-listener-zsgen5.service
</code></pre>
Reload daemon
<pre><code>sudo systemctl daemon-reload
</code></pre>
Start Docker event listener
<pre><code>sudo systemctl start docker-event-listener-zsgen5.service
</code></pre>
Status Docker event listener
<pre><code>sudo systemctl status docker-event-listener-zsgen5.service
</code></pre>
>active (running)

Enable Docker event listener
<pre><code>sudo systemctl enable docker-event-listener-zsgen5.service
</code></pre>
Verify Zigbee2MQTT
Reconnect USB sniffer
<pre><code>ls -al /dev/zsgen5
</code></pre>
>lrwxrwxrwx 1 root root 7 Sep 28 21:14 /dev/zsgen5 -> ttyACM0