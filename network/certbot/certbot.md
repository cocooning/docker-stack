## Certbot (Let's encrypt)
> Arrêter la stack __haproxy__. Certbot utilise le port 80
{.is-warning}

> Putty __sudo su__ pour effectuer toutes les opérations
{.is-warning}

Le répertoire __/.cocooning/data/certbot/etc/live/$ENV_SS_DOMAIN__ contiendra les certificats certbot __fullchain.pem__ et __privkey.pem__ qui seront concaténés et envoyés vers haproxy dans __/.cocooning/data/haproxy/__ (cron à faire) 

- Obtenir ou renouveler le certificat
> Vérifier l'hote et les ports de la box
{.is-warning}

> Avant d'obtenir un certificat il faut s'assurer que le sous-domaine a été créé sur OVH
{.is-warning}

```bash
docker run -it --rm --name certbot -p 80:80 -p 443:443 -v /.cocooning/data/certbot/etc:/etc/letsencrypt -v /.cocooning/data/certbot/lib:/var/lib/letsencrypt certbot/certbot:arm64v8-nightly certonly --standalone --email jp.tchube@cocooning.tech -d tchube.cocooning.tech --agree-tos
```

- Concaténer les certificats
```bash
sudo cat /.cocooning/data/certbot/etc/live/tchube.cocooning.tech/fullchain.pem /.cocooning/data/certbot/etc/live/tchube.cocooning.tech/privkey.pem | tee /.cocooning/data/haproxy/tchube.cocooning.tech.pem
```