---
title: ddclient
description: 
published: true
date: 2022-09-16T06:03:41.796Z
tags: 
editor: markdown
dateCreated: 2022-09-07T05:05:15.348Z
---
# ddclient
> Utiliser l'image __3.9.1__  
Voir problème de permission du fichier config sur l'images __latest__
## Pré-requis

- Volume ddclient créé
```bash
docker volume create ddclient
```

## Les variables d'environnement

```bash
${ENV_UID}
${ENV_GID}
${ENV_TZ}
${ENV_ADMIN_PASSWORD}  
${ENV_DDCLIENT_OVH_LOGIN_CLIENT}
${ENV_SYSLOG_IP}
${ENV_DDCLIENT_NODE_DEPLOY}
${ENV_SS_DOMAIN}
```

## Configuration

```bash
docker config create --template-driver golang ddclient /.cocooning/${ENV_SS_DOMAIN}/ddclient/ddclient.conf
```

## Installation

```bash
docker stack deploy --compose-file /.cocooning/docker-stack/network/ddclient/ddclient-docker-compose.yml ddclient
```





