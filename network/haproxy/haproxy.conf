# Configuration de haproxy
# docker config create --template-driver golang haproxy /.cocooning/${ENV_SS_DOMAIN}/haproxy/haproxy.conf

global
  log fd@2 local2
  user haproxy
  group haproxy
  stats socket /var/lib/haproxy/stats expose-fd listeners
  master-worker

resolvers docker
  nameserver dns1 127.0.0.11:53 # toujours la même adresse pour docker swarm DNS
  resolve_retries 3
  timeout resolve 1s
  timeout retry   1s
  hold other      10s
  hold refused    10s
  hold nx         10s
  hold timeout    10s
  hold valid      10s
  hold obsolete   10s

defaults
  mode http
  option httplog
  option redispatch
  option forwardfor # important pour les redirections d'un prefix
  timeout connect 4s # 4s
  timeout client 600s # 50s
  timeout server 600s # 50s
  stats enable
  stats auth {{env "ENV_ADMIN_LOGIN"}}:{{env "ENV_ADMIN_PASSWORD"}}
  stats refresh 5s
  stats show-node
  stats uri  /stats/haproxy  # https://ss-domain.cocooning.tech/stats/haproxy

frontend http_front
  log global
  bind :80

  acl http      ssl_fc,not
  http-request redirect scheme https if http

  bind *:443 ssl crt /usr/local/etc/haproxy/{{env "ENV_SS_DOMAIN"}}.cocooning.tech.pem
# admin
#  bind *:443 ssl crt /usr/local/etc/haproxy/tchube.cocooning.tech.pem crt /usr/local/etc/haproxy/wiki.cocooning.tech.pem crt /usr/local/etc/haproxy/influxdb.cocooning.tech.pem crt /usr/local/etc/haproxy/snipeit.cocooning.tech.pem crt /usr/local/etc/haproxy/grafana.cocooning.tech.pem

  acl domaine hdr(host) -i {{env "ENV_SS_DOMAIN"}}.cocooning.tech

# admin
#  acl sous-domaine-wiki hdr(host) -i wiki.cocooning.tech
#  acl sous-domaine-influxdb hdr(host) -i influxdb.cocooning.tech
#  acl sous-domaine-snipeit hdr(host) -i snipeit.cocooning.tech
#  acl sous-domaine-grafana hdr(host) -i grafana.cocooning.tech

  acl prefix_mosquitto path -i -m beg /mosquitto/
  acl prefix_portainer path -i -m beg /portainer/
  acl prefix_zigbee path -i -m beg /zigbee/

#  acl prefix_adminer path -i -m beg /adminer/  
#  acl prefix_vscode path -i -m beg /vscode/
#  acl prefix_syncthing path -i -m beg /syncthing/  
#  acl prefix_photoprism path -i -m beg /photoprism
#  acl prefix_semaphore path -i -m beg /semaphore
#  acl prefix_jellyfin path -i -m beg /jellyfin/
#  acl prefix_serviio path -i -m beg /console

  use_backend http_mosquitto if domaine prefix_mosquitto
  use_backend http_portainer if domaine prefix_portainer
  use_backend http_zigbee if domaine prefix_zigbee

#  use_backend http_adminer if domaine prefix_adminer
#  use_backend http_vscode if domaine prefix_vscode
#  use_backend http_syncthing if domaine prefix_syncthing  
#  use_backend http_photoprism if domaine prefix_photoprism
#  use_backend http_semaphore if domaine prefix_semaphore
#  use_backend http_jellyfin if domaine prefix_jellyfin
#  use_backend http_serviio if domaine prefix_serviio
#  use_backend http_wikijs if sous-domaine-wiki
#  use_backend http_influxdb if sous-domaine-influxdb
#  use_backend http_snipeit if sous-domaine-snipeit
#  use_backend http_grafana if sous-domaine-grafana

  default_backend http_{{env "ENV_HAPROXY_DEFAULT_BACKEND"}}


#######################################################################################
# Mode hhtp
#######################################################################################

backend http_hassio
  log global
  cookie cookiehassio insert indirect nocache # cookiehassio est le nom du cookie
  balance leastconn # voir correspondance mais à priori c'est le meilleur
#  balance roundrobin
  server hassio_1 192.168.1.100:8123 check resolvers docker cookie cookiehassio
#  server-template hassio 1 hassio_hassio:8123 check resolvers docker # inter 1000 cookie cookiehassio #tchube est la valeur du cookie


backend http_zigbee # Fonctionnement IP OK mais replicas > 1 et mode global ne fonctionnent pas
  log global
  http-request replace-path /zigbee/(.*) /\1 # Obligatoire tchube
  balance leastconn
#  server zigbee_1 192.168.1.102:8080 check #
  server zigbee_1 zigbee.cocooning.local:8080 check #

backend http_portainer # Fonctionnement IP OK mais replicas > 1 et mode global ne fonctionnent pas
  log global
  http-request replace-path /portainer/(.*) /\1 # Obligatoire tchube
  balance leastconn
  server portainer_1 portainer.cocooning.local:9000 check # une seule instance possible pour portainer mode swarm ? A priori oui...

backend http_mosquitto
  log global
  http-request set-path %[path,regsub(^/mosquitto/?,/)]
  balance leastconn
  server mosquitto_1 mosquitto.cocooning.local:8083 check

#backend http_photoprism
#  log global
##  http-request replace-path /photoprism(.*) /\1 # Ne pas mettre pour photoprism
#  balance leastconn
#  server photoprism_1 192.168.1.100:2342 check resolvers docker#

#backend http_vscode
#  log global
#  http-request replace-path /vscode/(.*) /\1 # Obligatoire tchube
#  balance leastconn
#  server vscode_1 192.168.1.101:8443 check #

#backend http_syncthing
#  log global
#  http-request replace-path /syncthing/(.*) /\1 # Obligatoire tchube
#  balance leastconn
#  server syncthing_1 192.168.1.102:8384 check #

#backend http_grafana
#  log global
#  balance leastconn
#  server grafana_1 192.168.1.101:3000 check #


#backend http_wikijs
#  log global
#  balance leastconn
#  server wikijs_1 192.168.1.100:3001 check #

#backend http_snipeit
#  log global
#  option forwardfor if-none
#  http-request set-header X-Forwarded-Port %[dst_port]
#  http-request add-header X-Forwarded-Proto https if { ssl_fc }
#  balance leastconn
#  server snipeit_1 192.168.1.101:8001 check #

#backend http_influxdb 
#  log global
#  balance leastconn
#  http-request replace-path /influxdb/(.*) /\1
#  server influxdb_1 192.168.1.101:8086 check


#backend http_adminer # Méthode fonctionne très bien
#  log global
#  http-request set-path %[path,regsub(^/adminer/?,/)]
#  balance leastconn
#  cookie cookieadminer insert # indirect nocache # cookieadminer est le nom du cookie
#  server-template adminer 1 adminer_adminer:8080 check resolvers docker cookie cookieadminer # inter 1000  #tchube est la valeur du cookie
##  server-template adminer 2 adminer_adminer:8080 check resolvers docker


#backend http_semaphore
#  log global
##  http-request replace-path /semaphore/(.*) /\1 # Obligatoire tchube
#  balance leastconn
#  server semaphore_1 192.168.1.102:3022 check resolvers docker #

#backend http_jellyfin
#  log global
##  option httpchk
##  option forwardfor
##  http-check send meth GET uri /health
##  http-check expect string Healthy
##  http-request replace-path /jellyfin/(.*) /\1 # Obligatoire tchube
#  balance leastconn
#  server jellyfin_1 192.168.1.100:8096 check resolvers docker#

#backend http_serviio
#  log global
##  option httpchk
##  option forwardfor
##  http-check send meth GET uri /health
##  http-check expect string Healthy
#  http-request replace-path /console(.*) /\1 # Obligatoire tchube
#  balance leastconn
#  server serviio_1 192.168.1.100:23423 check resolvers docker#

#######################################################################################
# Mode tcp 
#######################################################################################

#frontend tcp_zabbix # Voir backend
##  log global
#  bind *:10051
#  mode tcp
#  tcp-request inspect-delay 3s
#  use_backend tcp_zabbix

#backend tcp_zabbix
#  mode tcp # Répétition avec frontend utile
#  option tcp-check
#  balance roundrobin
#  server zabbix_1 192.168.1.101:10051 check

## Postgres
##  option pgsql-check user cocooning
##  option tcp-check
##  balance roundrobin
##  server postgres_1 192.168.1.100:5432 check

## Mariadb
##  option	mysql-check	user	cocooning
#  option tcp-check
#  balance roundrobin
#  server mariadb_1 192.168.1.100:3306 check



# garder le saut de ligne après



