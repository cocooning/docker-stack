---
title: haproxy
description: 
published: true
date: 2022-09-16T06:03:41.796Z
tags: 
editor: markdown
dateCreated: 2022-09-07T05:05:15.348Z
---
# haproxy
> Utiliser le plus possible le mode DNSR
- Version : lts-alpine3.19
## Pré-requis
> Fichier ${ENV_SS_DOMAIN}.cocooning.tech.pem présent
## Les variables d'environnement
```bash
${ENV_SYSLOG_IP}
${ENV_HAPROXY_NODE_DEPLOY}
${ENV_HAPROXY_DEFAULT_BACKEND}
```
## Configuration

```bash
docker config create --template-driver golang haproxy /.cocooning/${ENV_SS_DOMAIN}/haproxy/haproxy.conf
```

## Installation

```bash
docker stack deploy --compose-file /.cocooning/docker-stack/network/haproxy/haproxy-docker-compose.yml haproxy
```
## Post-installation 

