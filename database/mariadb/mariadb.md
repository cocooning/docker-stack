---
title: mariadb
description: 
published: true
date: 2022-09-16T06:03:41.796Z
tags: 
editor: markdown
dateCreated: 2022-09-07T05:05:15.348Z
---
# mariadb
- Hostname : mariadb.cocooning.local
> Pas de possibilité de création d'un volume NFS. A re-tester
## Pré-requis
- Répertoire __/.cocooning/data/mariadb__ sur le node de deploiement

## Les variables d'environnement
```bash
${ENV_SYSLOG_IP}
${ENV_MARIADB_NODE_DEPLOY}
${ENV_ADMIN_PASSWORD}
${ENV_ADMIN_LOGIN}
${ENV_TZ}
```
## Configuration
Si elle n'existe pas, la création de la première base de données __homeassistant__ se fait en utilisant le fichier de config __init.sql__. 

> Enlever les commentaires dans le compose

```bash
docker config create --template-driver golang mariadb /.cocooning/docker-stack/database/mariadb/init.sql
```
## Installation

```bash
docker stack deploy --compose-file /.cocooning/docker-stack/database/mariadb/mariadb-docker-compose.yml mariadb
```
## Post-installation (__A VERIFIER__)

### Création d'une BD

#### Config
#### Adminer
#### Command line dans le container

