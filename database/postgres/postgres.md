---
title: postgres
description: 
published: true
date: 2022-09-16T06:03:41.796Z
tags: 
editor: markdown
dateCreated: 2022-09-07T05:05:15.348Z
---
# postgres
- Version 14.5-alpine
- Hostname : postgres.cocooning.local
> Passer et tester la nouvelle version  
> Pas de possibilité de création d'un volume NFS. A re-tester
## Pré-requis
- Répertoire __/.cocooning/data/postgres__ sur le node de deploiement

## Les variables d'environnement
```bash
${ENV_SYSLOG_IP}
${ENV_POSTGRES_NODE_DEPLOY}
${ENV_ADMIN_PASSWORD}
${ENV_ADMIN_LOGIN}
${ENV_TZ}
```
## Configuration
Si elle n'existe pas, la création de la première base de données __??__ se fait en utilisant le compose

```bash
docker config create --template-driver golang postgres /.cocooning/docker-stack/database/postgres/postgresql.conf
```
## Installation

```bash
docker stack deploy --compose-file /.cocooning/docker-stack/database/postgres/postgres-docker-compose.yml postgres
```
## Post-installation (__A VERIFIER__)

### Création d'une BD

#### Config
#### Adminer
#### Command line dans le container

