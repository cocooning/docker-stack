---
title: influxdb1
description: 
published: true
date: 2022-09-16T06:03:41.796Z
tags: 
editor: markdown
dateCreated: 2022-09-07T05:05:15.348Z
---
# influxdb1
Version : 1.8  
> Tester la dernière version
> Changer les port 8083 (en conflit avec mosquitto) directement dans le config
## Pré-requis
- Répertoire __/.cocooning/data/influxdb1__ sur l'hote de deploiement
## Les variables d'environnement
```bash
${ENV_SYSLOG_IP}
${ENV_INFLUXDB1_NODE_DEPLOY}
${ENV_ADMIN_PASSWORD}
${ENV_ADMIN_LOGIN}
```
## Configuration
```bash
docker config create --template-driver golang influxdb1 /.cocooning/docker-stack/database/influxdb1/influxdb1.conf
```
## Installation

```bash
docker stack deploy --compose-file /.cocooning/docker-stack/database/influxdb1/influxdb1-docker-compose.yml influxdb1
```

## Databases 

Dans le container (console)

```bash
influx 
```
```bash
show database
create database <name> with duration 1d
drop database <name>
```
```bash
exit
```


