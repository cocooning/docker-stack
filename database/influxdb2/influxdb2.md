---
title: influxdb2
description: 
published: true
date: 2022-09-16T06:03:41.796Z
tags: 
editor: markdown
dateCreated: 2022-09-07T05:05:15.348Z
---
# influxdb2
> Tester la dernière version
## Pré-requis
- __cocooning-network__ créé
- variables d'environnement déclarées
- volmunes NFS montés
```bash
/.cocooning/data/influxdb2/lib
/.cocooning/data/influxdb2/etc
```

## Les variables d'environnement
```bash
${ENV_SYSLOG_IP}
${ENV_NFS_IP}
${ENV_INFLUXDB2_NODE_DEPLOY}
${ENV_ADMIN_PASSWORD}
${ENV_ADMIN_LOGIN}
```

## Installation

```bash
docker config create --template-driver golang influxdb2 /.cocooning/docker-stack/database/influxdb2/influxdb2.conf
```
```bash
docker stack deploy --compose-file /.cocooning/docker-stack/database/influxdb2/influxdb2-docker-compose.yml influxdb2
```

## Post-installation 

### Configuration par web UI

```bash
https://influxdb.cocooning.tech
```