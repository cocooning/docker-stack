---
title: grafana
description: 
published: true
date: 2022-09-16T06:03:41.796Z
tags: 
editor: markdown
dateCreated: 2022-09-07T05:05:15.348Z
---
# grafana

## Pré-requis

- volmune NFS monté
```bash
/.cocooning/data/grafana
```

## Les variables d'environnement
```bash
${ENV_SYSLOG_IP}
${ENV_GRAFANA_NODE_DEPLOY}
${ENV_ADMIN_PASSWORD}
${ENV_ADMIN_LOGIN}
```

## Installation

```bash
docker stack deploy --compose-file /.cocooning/docker-stack/monitoring/grafana/grafana-docker-compose.yml grafana
```
## Post-installation 

