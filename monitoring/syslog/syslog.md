---
title: syslog
description: 
published: true
date: 2022-09-16T06:03:41.796Z
tags: 
editor: markdown
dateCreated: 2022-09-07T05:05:15.348Z
---
# syslog
> Tester la mise en place du fichier config dans docker-stack . Le passage par une ENV de la variable __Destination IP de telegra__ ne fonctionne pas.

## Pré-requis
### Création des repertoires sur master1
```bash
/.cocooning/data/syslog
```
## Configuration

```bash
docker config create --template-driver golang syslog /.cocooning/${ENV_SS_DOMAIN}/syslog/syslog-ng.conf
```
## Installation

```bash
docker stack deploy --compose-file /.cocooning/docker-stack/monitoring/syslog/syslog-docker-compose.yml syslog
```

