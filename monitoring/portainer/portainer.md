---
title: Portainer server
description: 
published: true
date: 2022-09-16T06:03:41.796Z
tags: 
editor: markdown
dateCreated: 2022-09-07T05:05:15.348Z
---

> __Attention__ cette version n'affiche pas correctement l'appartenance des images aux nodes et leurs tags

# Portainer server
Version 2.20.1
> Une erreur sans conséquence se produit à la création de l'administrateur.  

## Pré-requis
- Répertoire __portainer__ dans __/.cocooning/data__ sur le node de déploiement

## Les variables d'environnement
```bash
${ENV_SYSLOG_IP}
${ENV_PORTAINER_NODE_DEPLOY}
```

## Installation du server

```bash
docker stack deploy --compose-file /.cocooning/docker-stack/monitoring/portainer/portainer-docker-compose.yml portainer
```
## Post-installation

- Settings
- Authentication : Modifier la longueur du mot de passe à __9 caractères__ 
- User-related
- Users : Changer le mot de passe dans settings

# Portainer Agent Edge
> Passer la communication de l'agent vers le server en https (haproxy ?)  

> Le nom de la stack déployée doit être portainer_edge_agent
Au final après deploiement le service sera utilisé pour la valeur de la variable d'environnement __AGENT_CLUSTER_ADDR: tasks.portainer_edge_agent__S
## Pré-requis

- Volume __portainer-agent__ créé

```bash
docker volume create portainer-agent
```

## Les variables d'environnement
Les variables __ENV_PORTAINER_EDGE_KEY__ et __ENV_PORTAINER_EDGE_ID__ sont uniques à l'environnement (cluster) deployé. 
Pour trouver les valeurs :
- Add environnements
- Swarm
- Start Wizard
- Name : <ss_domain>
- Portainer API : http://<IP>:9000 ou http://tchube.cocooning.tech:9000
- Create
- Créer les 2 variables d'environnement
- __close__

```bash
${ENV_PORTAINER_EDGE_KEY}
${ENV_PORTAINER_EDGE_ID}
```

## Installation
> Garder le nom de la stack __portainer_edge__ pour le nommage du service
```bash
docker stack deploy --compose-file /.cocooning/docker-stack/monitoring/portainer/edge-docker-compose.yml portainer_edge
```
