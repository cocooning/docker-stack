---
title: telegraf
description: 
published: true
date: 2022-09-16T06:03:41.796Z
tags: 
editor: markdown
dateCreated: 2022-09-07T05:05:15.348Z
---
# telegraf
> A deployer en https sur tchube
## Pré-requis
### Sur clster tchube
- Une instance d'influxdb1 (host, syslog)
- A voir, Une instance d'influxdb2 (docker, http)

## Les variables d'environnement

```bash
${ENV_SYSLOG_IP}
${ENV_ADMIN_PASSWORD}
${ENV_ADMIN_LOGIN}
${ENV_DOCKER_GID}
```

## Configuration
### telegraf-host
```bash
docker config create --template-driver golang telegraf-host /.cocooning/docker-stack/monitoring/telegraf/telegraf-host.conf
```
### telegraf-syslog
```bash
docker config create --template-driver golang telegraf-syslog /.cocooning/docker-stack/monitoring/telegraf/telegraf-syslog.conf
```
## Installation

```bash
docker stack deploy --compose-file /.cocooning/docker-stack/monitoring/telegraf/telegraf-docker-compose.yml telegraf
```

## Post-installation 

### Configuration par web UI
