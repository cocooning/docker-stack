---
title: serviio
description: 
published: true
date: 2022-09-16T06:03:41.796Z
tags: 
editor: markdown
dateCreated: 2022-09-07T05:05:15.348Z
---
# serviio
- Mode host
> A installer sur le node __hassio__
> La creation d'un volume permet de sauvegarder les url pour les __media_content_id__ d'hassio

## Pré-requis

- Volume serviio créé __sur le node deploy__
```bash
docker volume create serviio
```
Repertoires créés
- /.cocooning/data/serviio/media
- /.cocooning/data/serviio/config
- /.cocooning/data/serviio/library
- /.cocooning/data/serviio/plugins
- /.cocooning/data/serviio/log

## Configuration
- /.cocooning/data/serviio/config/profiles.xml
- /.cocooning/data/serviio/config/application-profiles.xml

## Installation

Sur le node __hassio__

```bash
cd /.cocooning/data/serviio
```
- Recopier le contenu du compose __serviio-docker-compose.yml__ dans __docker-compose.yml__

```bash
docker compose up
```
## Post installation

Redemarrer le container (portainer)

