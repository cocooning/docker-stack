# go2rtc

## Pré-requis

- volmune NFS monté
```bash
/.cocooning/data/grafana
```

## Les variables d'environnement
```bash
${ENV_SYSLOG_IP}
${ENV_GRAFANA_NODE_DEPLOY}
${ENV_ADMIN_PASSWORD}
${ENV_ADMIN_LOGIN}
```

## Installation

```bash
docker stack deploy --compose-file /.cocooning/docker-stack/monitoring/grafana/grafana-docker-compose.yml grafana
```
## Post-installation 

