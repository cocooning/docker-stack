---
title: bubbleupnp
description: 
published: true
date: 2022-09-16T06:03:41.796Z
tags: 
editor: markdown
dateCreated: 2022-09-07T05:05:15.348Z
---
# bubbleupnp
- Mode host
> A installer sur le node __hassio__
> La creation d'un volume permet de sauvegarder les __media_content_id__

## Pré-requis


- Volume bubbleupnp créé __sur le node deploy__
```bash
docker volume create bubbleupnp
```
Repertoires créés
- /.cocooning/data/bubbleupnp/media


## Configuration
- /.cocooning/data/bubbleupnp/configuration.xml

## Installation

Sur le node __hassio__

```bash
cd /.cocooning/data/bubbleupnp
```
- Recopier le contenu du compose __bubbleupnp-docker-compose.yml__ dans __docker-compose.yml__

```bash
docker compose up
```
## Post installation

```bash
http://192.168.1.100/58050
```
Modifier dans __Network and Security__
- login
- Password
- Public host name <ss_domain>.cocooning.tech
