## Docker

### Désinstallation d'une version existante
- Sur __chaque hote__ du cluster (worker)
```bash
docker swarm leave
```
- Désinstaller les paquets "docker"
> Ajuster la ligne de commande ci-dessous en fct des erreurs
```bash
sudo apt-get purge docker.io docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin
```
Le répertoire /var/lib/docker/ doit être supprimé (suppression des images, des conteneurs, des volumes des réseaux).
```bash
sudo rm -rf /var/lib/docker
sudo rm -rf /var/lib/containerd
```

### Installation docker-ce
> __ATTENTION__ ne pas installer __docker-compose__ pour le moment. Son installation provoque la réinstallation de docker (version du dépôt de l'OS)  

> Si echec de l'install, vérifier les dépots __/etc/apt/sources.list.d__ et __sources.list__
```bash
sudo su
cd /home/tchube
curl -fsSL https://get.docker.com -o get-docker.sh
```

#### Installer la dernière version
```bash
sudo sh get-docker.sh --channel stable
```

### Création du cluster SWARM
- sur master1
```bash
docker swarm init --advertise-addr xxx.xxx.x.xx
```
- Obtenir les __join-token__ pour nouveaux worker et manager

```bash
docker swarm join-token worker
docker swarm join-token manager
```

## Création du network __cocooning-network__
```bash
docker network create -d overlay --attachable cocooning-network
```
NFS

## Les variables d'environnement

sur chaque hote
Elles seront  préfixées par __ENV__. et peuvent être différentes d'un hote à l'autre

```bash
sudo nano /etc/environment
```
```bash
for line in $(cat /etc/environment) ; do export $line ; done
```
La prise en compte des nouvelles variables se fera immédiatement et seront disponibles après __reboot__

visualisation des variables d'environnement

```bash
printenv 
printenv | grep ENV_
```

## Ordre de déloiement du cluster de base
- ddclient (on peut maintenant utiliser vscode web)
- portainer
- certbot
- haproxy

zigbee -> mosquitto
- mosquitto
- zigbee

Hassio -> database
- mariadb, postgres (pour hassio)
- hassio

Monitoring (sur tchube)
syslog -> telegraf -> influxdb1  -> grafana

L'ordre de création :
- influxdb1 (sur tchube uniquement)
- telegraf (host, syslog)
- syslog
- grafana (sur tchube uniquement)

telegraf - influxdb2 -> grafana
  - grafana
  - infuxdb2
  - telegraf


<h1 style="color:#5434D7">La stack</h1>

<h2 style="color:#5434D7">Les noeuds</h2>

<h3 style="color:#5434D7">Les commandes</h3>

```bash
docker node demote ID # Demote one or more nodes from manager in the swarm. Le noeud passe de manager à worker
docker node inspect	# Display detailed information on one or more nodes
docker node ls	# List nodes in the swarm
docker node promote	# Promote one or more nodes to manager in the swarm
docker node ps	# List tasks running on one or more nodes, defaults to current node
docker node rm	# Remove one or more nodes from the swarm
docker node update	# Update a node
docker system prune # ne supprime pas les volumes. 
docker system prune -a -f --volumes # supprimera des volumes. 
docker system prune -af --volumes # nettoiera toutes les ressources docker créées auparavant.
```

<h3 style="color:#5434D7">Les labels - tags</h3>

L'installation des containers se fera sur les nodes taggés du nom de l'application

```bash
docker node update --label-add foo --label-add bar worker1
docker node update --label-add foo=foox --label-add bar=barx worker1

docker node update --label-rm foo
```

Utilisation des labels dans le déploiement

```bash
    deploy:
      mode: replicated
      replicas: 1
      placement:
        constraints:
          - node.labels.hassio == true
```

<h2 style="color:#5434D7">Les images</h2>

Le numéro de version est important pour des mises à jours maîtrisées.  
Les dépendances (dockerize)  
Les opérations vers le registre docker hub  

<h2 style="color:#5434D7">Les variables d'environnement</h2>


 * Pour portainer 
 
 utiliser le fichier environnement spécifique à l'utilisateur (sous-domaine) __/.cocooning/data/environment/${ENV_SS_DOMAIN}.env__

```bash
for line in $( cat /.cocooning/data/environment/${ENV_SS_DOMAIN}.env) ; do export $line ; done
``` 

Pour la prise en compte des variables d'environnement dans l'environnement "portainer" il faut mettre à jour les variables dans le service portainer : copier/coller __{ENV_SS_DOMAIN}.env__ (advanced mode)

Dans les app du template.json utiliser :

```bash
			"env": [
				{
                    "name": "ENV_ENVIRONMENT_FILE",
                    "label": "Environment file",
					"description": "Nom du fichier d'environnement $ENV_SS_DOMAIN"
                } 
			]
```

* soit de manière ponctuelle

```bash
export ENV_foo=foox
```

Pour lister les variables d'envionnement préfixées

```bash
printenv | grep ENV_
```

<h2 style="color:#5434D7">Les secrets</h2>

```bash
printf "my_password" | docker secret create tchube -
```

```bash
    secrets:
      - source: tchube
        target: /tchube/secret
```

```bash
    command:
        - 'export ENV_ADMIN_PASSWORD=oauat7579'
```

```bash
      - ENV_ADMIN_PASSWORD=tchube/secret
```

```bash
secrets:
  tchube:
    external: true
```

> A affiner. pour le moment des mot de passes seront stockés en tant que variable d'environnement.

<h2 style="color:#5434D7">La gestion des logs - logging</h2>

<h3 style="color:#5434D7">Créer des logs toutes les 30 secondes</h3>

```bash
sudo docker run --hostname container0 --name container0 -it ubuntu
```

Dans le container0

```bash
c=0; while true; do echo "$c: $(date) $HOSTNAME"; c=$((c+1)); sleep 60; done
```

> Créer un docker-compose avec le driver syslog, modifier le container avec Portainer

<h3 style="color:#5434D7">Gerer les logs via le server syslog</h3>

Le server syslog-ng est installé sur un noeud (label syslog). Le driver syslog peut être installé soit globalement pour l'ensemble des services du Swarm soit pour chaque service déployé  
Port UDP par défaut : 514  
Noton que le protocole TCP ne fonctionne pas avec le server __syslog-ng__

<h4 style="color:#5434D7">Déclaration globale</h4>

Le logging driver est défini de manière générale dans le fichier
 __/etc/docker/daemon.json__ sur chaque noeud.
 
```bash
{
  "log-driver": "syslog",
  "log-opts": {
    "syslog-address": "udp://SYSLOG_IP:SYSLOG_PORT"
  }
}
```

Relancer le service docker

```bash

```

<h4 style="color:#5434D7">Déclaration dans un service</h4>

Cette manière permet d'ajuster individuellement le driver au service

* none
* local
* json-file
* syslog
* journald

mais également de tagger les logs.

```bash
    logging:
      driver: "syslog"
      options:
      syslog-address: "udp://${ENV_SYSLOG_IP}:${ENV_SYSLOG_PORT}"
      tag: "ddclient"
```

<h4 style="color:#5434D7">Connaitre le pilote de journalisation</h4>

Pilote d'un container
```bash
docker inspect -f '{{.HostConfig.LogConfig.Type}}' <CONTAINER>
```

Pilote de journalisation global de docker

```bash
docker info --format '{{.LoggingDriver}}' 
```

<h2 style="color:#5434D7">Les fichiers de configuration - config</h2>

Par définition un fichier de configuration est statique (pas de mise à jour sur disque une fois créé). Il sera déployé (et dupliqué) automatiquement en fonction du mode choisi (global, duplicated).

Les fichiers de configurations se trouveront dans le répertoire __/.cocooning/user/config__ et seront accessibles avec VSCODE

Le fichier de configuration sera créé en utilisant le template driver GOLANG pour la mise à jour dynamique des variables. 

```bash
docker config create --template-driver golang ddclient /.cocooning/${ENV_SS_DOMAIN}/config/ddclient.conf
```
  
Les variables d'environnement seront passées dans le docker-compose sous la forme : 

```bash
    environment:
      - ENV_DOMAIN=${ENV_DOMAIN}
      - ENV_SS_DOMAIN=${ENV_SS_DOMAIN}
```
et accessibles dans le fichier docker __config template golang__ sous la forme :

```bash
{{env "ENV_SS_DOMAIN"}}
```

<h2 style="color:#5434D7">Le réseau - Network</h2>

```bash
docker network create -d overlay cocooning-network
```

> Affiner la création

<h2 style="color:#5434D7">Les volumes</h2>

Plusieurs types de volume utilisés fonction du contexte avec l'objectif principal de limiter le plus possible les écritures sur la SD et utiliser le répertoire __data__ du DD externe

<h3 style="color:#5434D7">Bind "data"</h3>

Un volume monté de type __bind__ généralement sur le répertoire __data__ est utilisé. Le container sera exécuté sur ce même noeud pour accéder à data.  

<h4 style="color:#5434D7">Avantages</h4>

* Réduire les ecriture sur le SD en ecrivant sur le SSD (hôte)
* Accéder aux fichiers de données via le répertoire de l'hôte (data)

<h4 style="color:#5434D7">Inconvénients</h4>

* Le container doit être exécuté exclusivement sur l'hôte __data__

<h3 style="color:#5434D7">Volume externe nommé</h3>

Le volume sera stocké dans /etc/docker et sera dupliqué sur chaque noeud exécutant le container. Les écritures devront être peu fréquentes sur la SD.

<h3 style="color:#5434D7">Data Volume Container</h3>

> A voir

<h3 style="color:#5434D7">tmpfs</h3>

> A voir

<h3 style="color:#5434D7">NFS</h3>

<h4 style="color:#5434D7">Avantages</h4>

* Réduire les ecriture sur le SD en ecrivant sur le SSD
* Donner aux services la possibilité d'être exécutés sur n'importe quel noeud
* Accéder aux fichiers de données via le répertoire de l'hôte (data)

<h4 style="color:#5434D7">Inconvénients</h4>

* A tester, mais peut-être des pbs de performance notamment dans le mode __sync__ du server

Ex le repertoire config de homeassistant peut être accessible en utilisant une instance HA sur le worker1 ou le master1

__Ne pas utiliser de serveur NFS pour partager une BD__. Dans le cas d'un haute disponibilité l'utilisation de Mariadb galera cluster est nécessaire

<h4 style="color:#5434D7">Montage d'un serveur NFS</h4>

L'exécution du server se fera sur l'hôte portant le volume __data__ (déclaration dans fsatb)

```bash
sudo nano /etc/exports
```

```bash
/.cocooning/data/mosquitto/data HOTE_IP/24(rw,no_root_squash,async,no_subtree_check)
```

```bash
sudo exportfs -ra
```

```bash
sudo service nfs-kernel-server reload
```

<h4 style="color:#5434D7">Déclaration du volume dans la stack</h4>

```bash
volumes:
  homeassistant:
    driver: local
    driver_opts:
      type: "nfs4"
      o: "nfsvers=4,addr=$ENV_NFS_IP,rw,soft,nolock"
      device: ":/.cocooning/$ENV_SS_DOMAIN/homeassistant/hassio/"
```

If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.

<h2 style="color:#5434D7">Les dépendances</h2>

